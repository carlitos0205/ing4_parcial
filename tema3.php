<?php

define("TAMANO_MATRIZ", 3); 

function generarMatrizAleatoria($tamano) {
    $matriz = array();
    for ($i = 0; $i < $tamano; $i++) {
        $fila = array();
        for ($j = 0; $j < $tamano; $j++) {
            $fila[] = rand(0, 9);
        }
        $matriz[] = $fila;
    }
    return $matriz;
}

function imprimirMatriz($matriz) {
    foreach ($matriz as $fila) {
        echo implode("\t", $fila) . PHP_EOL;
    }
}

function sumaDiagonalPrincipal($matriz) {
    $suma = 0;
    for ($i = 0; $i < count($matriz); $i++) {
        $suma += $matriz[$i][$i];
    }
    return $suma;
}

do {
    $matriz = generarMatrizAleatoria(TAMANO_MATRIZ);
    echo "Matriz:" . PHP_EOL;
    imprimirMatriz($matriz);
    $sumaDiagonal = sumaDiagonalPrincipal($matriz);
    echo "Suma de la diagonal principal: " . $sumaDiagonal . PHP_EOL;

    if ($sumaDiagonal >= 10 && $sumaDiagonal <= 15) {
        echo "La suma de la diagonal está entre 10 y 15." . PHP_EOL;
        break;
    } else {
        echo "La suma de la diagonal no está entre 10 y 15." . PHP_EOL;
    }
} while (true);
?>
